# Etagere Parametrique

Une simple étagère, usinable à la CN ou au Laser (pour stocker les filaments de PLA).

Les dimensions sont ajustables dans une feuille de calcul (largeur en nombre de boites, profondeur, hauteur de la bordure, diamètre des vis de fixations, équerre).

![alt text](xport/anim.gif)